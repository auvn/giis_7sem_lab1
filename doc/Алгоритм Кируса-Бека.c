// Cyrus-Beck 2-D Line Clipping algorithm
// for ease of coding we will treat a 2D point and a 2D vector
// as the same 
struct Point2D {
	float x,y;
}
// for simplicity we set an upper bound on the number of 
// points allowed to define a polygon - by moving to a class
// with a constructor we could make the array any size we wanted
const int MAXP = 100;
struct Polygon {
	int nPoints;
	Point2D v[MAXP];
}
const int MAXN = 100;
typedef Point2D Normal[MAXN];

// compute the outer normals.  
// note that this requires that the polygon be convex
// to always work
void CalcNormals (Polygon p, Normal & n)
{ int i,j,k;
  point2D v;
  for (i = 0; i < p.nPoints; i++)
  { j = (i+1)%p.nPoints;
    k = (i+2)%p.nPoints;
    // make vector be -1/mI + 1J
    n[i].x = -(p.v[j].y - p.v[i].y)/(p.v[j].x - p.v[i].x);
    n[i].y = 1.0;
    v.x = p.v[k].x - p.v[i].x;
    v.y = p.v[k].y - p.v[i].y;
    if (DotProduct (n[i],v) > 0)    // inner normal
    { n[i].x *= -1;
      n[i].y  = -1;
    }
  }
}
    
float DotProduct (Point2D v1, Point2D v2)
{ 
   return v1.x*v2.x + v1.y*v2*y;
}

void CBClip (Point2D p1, Point2D p2, Normal n, Polygon p, Boolean & visible,
                     Point2D & rp, Point2D & q)
{ float t1,t2,t,num,den;
  Point2D dirV,F;          // vectors
  int I;
  
    // start largest at smallest legal value and smallest 
    // at largest legal value
  t1 = 0.0;
  t2 = 1.0;
   // compute the direction vector
  dirV.x = p2.x - p1.x;
  dirV.y = p2.y - p1.y;

  visible = TRUE;
  i = 0;
  while ( (i < p.nPoints) && visible)
  { F.x = p1.x - p.v[i].x;
    F.y = p1.y - p.v[i].y;
    
    num  = DotProduct (n[i],F);
    den   =  DotProduct (n[i],dirV);

    if (den == 0.0)          // Parallel or Point
    { // parallel - if outside then forget the line; if inside then there are no 
      // intersections with this side 
      // but there may be with other edges, so in this case just keep going
       if (num > 0.0)
        visible = FALSE;   //   Parallel and outside or point (p1 == p2) and outside
    }
    else 
    { t = -(num/den);
      if (den < 0.0)        // entering
      { if (t <= 1.0)
          if (t > t1)
            t1 = t;
      }
      else if ( t >= 0.0)    //exiting
        if (t < t2)
          t2 = t;
    }
    i++;
  }
  if ( t1 <= t2)
  { rp.x = p1.x + t1*dirV.x;
    rp.y = p1.y + t1*dirV.y;
    q.x = p1.x + t2.dirV.x
    q.y = p1.y + t2*dirV.y
  }
  else
    visible = FALSE;
}