﻿var CartesianWorkplace = {
	prototype : WorkPlace,
	
	centerX : 0,
	centerY : 0,
	
	translate : function (physPoint) {
		var px = physPoint.x;
		var py = physPoint.y;

		var rx = parseInt(px / (this.scale + this.offset()));
		var ry = parseInt(py / (this.scale + this.offset()));

		return new Point(rx - this.centerX, this.centerY - ry);
	},

	create : function (width, height) {
		this.prototype.create(width, height);
		
		this.centerX = parseInt(this.width / 2);
		this.centerY = parseInt(this.height / 2);

		console.log("Center at: " + this.centerX + "x" + this.centerY);
	},
	
	getPixelColor : function (x, y) {
		return this.workArea[this.centerX + x][this.centerY - y].color;
	},
	
	putPixel : function (x, y, color) {
		this.prototype.putPixel(this.centerX + x, this.centerY - y, color);
	}
};