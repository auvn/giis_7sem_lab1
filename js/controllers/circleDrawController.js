﻿var circleDrawCtrl = {

    points: [new Point3D(0, 0, 0)],

    paramR: 10,

    draw: function (debug) {
        var c = this.points[0];

        if (debug) {
            this.debug = app.drawer.circleDebug(c, this.paramR);
            app.startDebug();
        }
        else {
            var drawedPoints = app.drawer.circle(this.points[0], this.paramR);
            app.drawer.circleDraw(drawedPoints);
            if (this.debug) {
                app.stopDebug();
            }
        }

        WorkPlace.repaint();
    },

    translate: function (dx, dy, dz) {
        this.points[0] = this.points[0].translate(dx, dy, dz);
    },

    rotate: function (ax, ay, az) {

    },

    redraw: function () {
        this.clear();
        app.drawer.circle(this.points[0], this.paramR);
        WorkPlace.repaint();
    },

    // set center point
    setPoint1: function (x, y) {
        this.points[0] = new Point3D(x, y, 0);
    },

    // set radius as length between the center and the input point
    setPoint2: function (x, y) {
        var cx = this.points[0].x;
        var cy = this.points[0].y;
        var dx = Math.abs(cx - x);
        var dy = Math.abs(cy - y);
        var r = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        this.paramR = Math.round(r);
    },
    clear: function () {
        baseDrawer.clear();

    }
};

