var clipperCtrl = {
	
	points: [],
	
	enableDrawInvisibleParts: true,
	
	INVISIBLE_LINE: -1,
	
	algName: "cs",
	
    draw: function () {
		
        var p1 = this.points[0];
        var p2 = this.points[1];		
		
		if (this.enableDrawInvisibleParts) {
			baseDrawer.baseDrawColor = Colors.pink;
			app.drawer.line(p1, p2);
			baseDrawer.baseDrawColor = Colors.black;
		}
		
		var result, p1new, p2new;
		switch (this.algName) {
            case "cs":
                result = this.clipper.clip(this.r, p1, p2);
				p1new = p1;
				p2new = p2;
                break;
            case "cb":
                p1new = new Point3D();
				p2new = new Point3D();
				result = this.clipper.clip(this.r, p1, p2, p1new, p2new);
                break;
        }
		if (result != this.INVISIBLE_LINE) {
			p1new.toInt();
			p2new.toInt();
			app.drawer.line(p1new, p2new);
		}
		
        WorkPlace.repaint();
    },


    setPoint1: function (x, y) {
        this.points[0] = new Point3D(x, y, 0);
    },

    setPoint2: function (x, y) {
        this.points[1] = new Point3D(x, y, 0);
    },

    clear: function () {
        baseDrawer.clear();
    },
	
	createRect: function () {
		var x_max = parseInt(app.parametersDoc.getElementById("x_max").value);
		var y_max = parseInt(app.parametersDoc.getElementById("y_max").value);
		var x_min = parseInt(app.parametersDoc.getElementById("x_min").value);
		var y_min = parseInt(app.parametersDoc.getElementById("y_min").value);
		
		var rectP1 = new Point(x_min, y_min);
		var rectP2 = new Point(x_min, y_max);
		var rectP3 = new Point(x_max, y_max);
		var rectP4 = new Point(x_max, y_min);
		
		baseDrawer.baseDrawColor = Colors.pink;		
		app.drawer.line(rectP1, rectP2);
		app.drawer.line(rectP2, rectP3);
		app.drawer.line(rectP3, rectP4);
		app.drawer.line(rectP4, rectP1);		
		baseDrawer.baseDrawColor = Colors.black;
		
		this.r = new rect(x_min, y_min, x_max, y_max); 
		WorkPlace.repaint();
	},
	
	createPolygon: function () {
		var p1 = new Point(30, 60);
		var p2 = new Point(20, 40);
		var p3 = new Point(40, 30);
		var p4 = new Point(60, 40);
		var p5 = new Point(50, 60);
		
		baseDrawer.baseDrawColor = Colors.pink;		
		app.drawer.line(p1, p2);
		app.drawer.line(p2, p3);
		app.drawer.line(p3, p4);
		app.drawer.line(p4, p5);
		app.drawer.line(p5, p1);		
		baseDrawer.baseDrawColor = Colors.black;
		
		this.r = [p1, p2, p3, p4, p5];
		WorkPlace.repaint();		
	},
	
	selectAlgorithm: function (algName) {
        switch (algName) {
            case "cs":
                this.clipper = cohSuthClipper;
				this.algName = "cs";
				this.createRect();
                break;
            case "cb":
                this.clipper = cyrusBeckClipper;
				this.algName = "cb";
				this.createPolygon();
                break;
        }
    },
	
	drawInvisibleParts: function (enableFlag) {
		this.enableDrawInvisibleParts = enableFlag;		
	}
};

