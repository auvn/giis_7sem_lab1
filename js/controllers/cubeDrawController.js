﻿var cubeDrawCtrl = {

	//points: [],	
	
	points : [
		new Point3D(-1,1,-1,1),
		new Point3D(1,1,-1,1),
		new Point3D(1,-1,-1,1),
		new Point3D(-1,-1,-1,1),
		new Point3D(-1,1,1,1),
		new Point3D(1,1,1,1),
		new Point3D(1,-1,1,1),
		new Point3D(-1,-1,1,1)
	],
	
	x : 70,
	y : 70,
	z : 3,
	f : 50,
		
    draw: function (debug) {
		
        var x = parseFloat(app.parametersDoc.getElementById("x").value);
        var y = parseFloat(app.parametersDoc.getElementById("y").value);
		var z = parseFloat(app.parametersDoc.getElementById("z").value);
        var f = parseFloat(app.parametersDoc.getElementById("f").value);
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.f = f;
		
		app.drawer.cube(this.points, this.x, this.y, this.z, this.f,true);

        WorkPlace.repaint();
    },
	
	translate: function (dx, dy, dz) {
		for (var i = 0; i < this.points.length; i++) {
			this.x += dx;
			this.y += dy;
			this.z += dz;
		}
	},
	
	scale: function (sx, sy, sz) {
		var newPoints = [];
		for (var i = 0; i < this.points.length; i++) {
			var p = this.points[i].scale(sx, sy, sz);
			if (p.z < this.z) {
				newPoints.push(p);
			}
			else {
				return;
			}			
		}
		this.points = newPoints;
	},
	
	rotate: function (ax, ay, az) {	
		var newPoints = [];
		for (var i = 0; i < this.points.length; i++) {
			var p = this.points[i].rotateX(ax).rotateY(ay).rotateZ(az);
			if (p.z < this.z) {
				newPoints.push(p);
			}
			else {
				return;
			}			
		}
		this.points = newPoints;
	},
	
	redraw: function() {
		WorkPlace.clear();
		app.drawer.cube(this.points, this.x, this.y, this.z, this.f,true);
        WorkPlace.repaint();
	},

    // set the start point
    setPoint1: function (x, y) {
        app.parametersDoc.getElementById("x").value = x;
        app.parametersDoc.getElementById("y").value = y;
    },

    setPoint2: function (x, y) {

    },
	
    clear: function () {
        baseDrawer.clear();
    }
};

