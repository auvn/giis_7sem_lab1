var curvesDrawCtrl = {

    points: [],
    pointsLimit: -1,
    selectedPoint: null,
    isMove: false,
    isDrawed: false,


    draw: function (debug) {
        this.isDrawed = true;
        app.drawer.curve(this.points, this.isMove);
        baseDrawer.plotPoints(this.points, Colors.pink);
        WorkPlace.repaint();
    },

    setPoint1: function (x, y) {
        var point = new Point(x, y);

        this.points.push(point);
        app.parametersDoc.getElementById("pointsCount").innerHTML = this.points.length;
    },

    setPoint2: function (x, y) {
    },

    movePoint1: function (point, newPoint) {
        // трабла, когда не попадаешь по пикселю при перемещении , он тянет предыдущий запомненный
        if (this.isDrawed) {

            if (equalPoints(point, newPoint) == true) {
                var checkedPoint = this.isCheckedPoint(point);
                if (checkedPoint != null) {
                    this.selectedPoint = checkedPoint;
                } else {
                    this.selectedPoint = null;
                    return false;
                }
            } else if (this.selectedPoint != null) {
                baseDrawer.clear();
                this.selectedPoint.x = newPoint.x;
                this.selectedPoint.y = newPoint.y;
                this.draw(null);
                return true;

            }
        }
        return false;
    },

    isCheckedPoint: function (point) {
        for (var i = 0; i < this.points.length; i++) {
            var checkedPoint = this.points[i];
            if (equalPoints(point, checkedPoint)) {
                return checkedPoint;
            }

        }
        return null;
    },

    clear: function () {
        baseDrawer.clear();
        this.isDrawed = false;
        this.clearPoints();
    },

    selectAlgorithm: function (algName) {
        console.log("selected " + algName + " algorithm");
        switch (algName) {
            case "bezye":
                app.drawer = bezyeDrawer;
                break;
            case "ermit":
                app.drawer = ermitDrawer;
                break;
            case "b-splines":
                app.drawer = bSplinesDrawer;
                break;
        }
    },
    clearPoints: function () {
        delete this.points;
        this.points = [];
    }
}

