var equationDrawCtrl = {

    typeFun: null,


    max: 90,

    selectType: function (type) {
        this.typeFun = this[type];
    },

    mainLine: function () {
        var points = [];
        var x1 = parseFloat(app.parametersDoc.getElementById("x11").value);
        var x2 = parseFloat(app.parametersDoc.getElementById("x22").value);

        var y1 = parseFloat(app.parametersDoc.getElementById("y11").value);
        var y2 = parseFloat(app.parametersDoc.getElementById("y22").value);


        return brezLineDrawer.line(new Point(x1, y1), new Point(x2, y2));
    },

    twolines: function () {
        var points = [];

        var x1 = parseFloat(app.parametersDoc.getElementById("x111").value);
        var x2 = parseFloat(app.parametersDoc.getElementById("x222").value);

        var y1 = parseFloat(app.parametersDoc.getElementById("y111").value);
        var y2 = parseFloat(app.parametersDoc.getElementById("y222").value);

        return brezLineDrawer.line(new Point(x1, y1), new Point(x2, y2));
    },
    line_and_circle: function () {

        var a = parseFloat(app.parametersDoc.getElementById("a3").value);
        var b = parseFloat(app.parametersDoc.getElementById("b3").value);
        var R = parseFloat(app.parametersDoc.getElementById("R").value);

        var points = circleDrawer.circle(new Point(a, b), R)
        return points;
    },

    line_and_parabola: function () {
        var points = [];

        var a = parseFloat(app.parametersDoc.getElementById("a4").value);
        var b = parseFloat(app.parametersDoc.getElementById("a4").value);
        var p = parseFloat(app.parametersDoc.getElementById("p4").value);

        points = parabolaDrawer.parabola(new Point(a, b), p);

        return points;
    },


    draw: function (debug) {
        var linePoints = this.mainLine();
        var points1 = [];
        if (this.typeFun == null)
            points1 = this.twolines();
        else
            points1 = this.typeFun();


        var result = equationDrawer.drawAndCalculate(linePoints, points1);

        this.showResult(result);

        WorkPlace.repaint();
    },

    showResult: function (points) {
        var answerField = app.parametersDoc.getElementById("resolution");

        var result = [];
        var str = "";
        for (var i = 0; i < points.length; i++)
            if (this.find(result, points[i]) == false) {
                result.push(points[i]);
                str += points[i].x + ";" + points[i].y + " "
            }

        baseDrawer.plotPoints(result, Colors.pink);

        answerField.innerHTML = str;


    },
    find: function (points, point) {
        for (var i = 0; i < points.length; i++) {
            if (point.x == points[i].x && point.y == points[i].y)
                return true;
        }
        return false;
    },

    setPoint1: function (x, y) {
        app.parametersDoc.getElementById("x11").value = x;
        app.parametersDoc.getElementById("y11").value = y;
    },
    setPoint2: function (x, y) {
        app.parametersDoc.getElementById("x22").value = x;
        app.parametersDoc.getElementById("y22").value = y;
    },
    clear: function () {
        baseDrawer.clear();
    }


}