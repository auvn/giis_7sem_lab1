var lineDrawCtrl = {
    points: [],
    draw: function (debug) {


        var p1 = this.points[0];
        var p2 = this.points[1];

        if (debug) {
            this.debug = app.drawer.lineDebug(p1, p2);
            app.startDebug();
        }
        else {
            app.drawer.line(p1, p2);
            if (this.debug) {
                app.stopDebug();
            }
        }

        WorkPlace.repaint();
    },

    translate: function (dx, dy, dz) {
        this.points[0] = this.points[0].translate(dx, dy, dz);
        this.points[1] = this.points[1].translate(dx, dy, dz);
    },

    selectAlgorithm: function (algName) {
        console.log("selected " + algName + " algorithm");
        switch (algName) {
            case "cda":
                app.drawer = cdaLineDrawer;
                break;
            case "brez":
                app.drawer = brezLineDrawer;
                break;
            case "wu":
                app.drawer = wuLineDrawer;
                break;
        }
    },

    rotate: function (a) {
        this.points[1] = this.points[1].rotate(a);
    },

    setPoint1: function (x, y) {
        this.points[0] = new Point3D(x, y, 0);
    },

    setPoint2: function (x, y) {
        this.points[1] = new Point3D(x, y, 0);
    },

    redraw: function () {
        this.clear();
        app.drawer.line(this.points[0], this.points[1]);
        WorkPlace.repaint();
    },

    clear: function () {
        baseDrawer.clear();

    }
};

