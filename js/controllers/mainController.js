var previousColor;

var app = {

    ctrl: null,
    drawer: null,
    parametersDoc: null,

    setLineMode: function () {
        this.ctrl = lineDrawCtrl;
        this.drawer = cdaLineDrawer;
        document.getElementById(Namespace.parameters).src = "partials/lines.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим линий";
    },

    setCircleMode: function () {
        this.ctrl = circleDrawCtrl;
        this.drawer = circleDrawer;
        document.getElementById(Namespace.parameters).src = "partials/circles.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим окружностей";
    },

    setParabolaMode: function () {
        this.ctrl = parabolaDrawCtrl;
        this.drawer = parabolaDrawer;
        document.getElementById(Namespace.parameters).src = "partials/parabolas.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим парабол";
    },

    setCurvesMode: function () {
        this.ctrl = curvesDrawCtrl;
        this.drawer = bezyeDrawer;
        document.getElementById(Namespace.parameters).src = "partials/curves.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим кривых второго порядка";
    },

    setCubeMode: function () {
        this.ctrl = cubeDrawCtrl;
        this.drawer = cubeDrawer;
        document.getElementById(Namespace.parameters).src = "partials/cubes.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим параллелепипедов";
    },

    setTrianglesMode: function () {
        this.ctrl = trianglesDrawCtrl;
        this.drawer = trianglesDrawer;
        document.getElementById(Namespace.parameters).src = "partials/triangles.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим триангуляции";
    },

    setClippingMode: function () {
        this.ctrl = clipperCtrl;
        this.drawer = brezLineDrawer;
        this.ctrl.clipper = cohSuthClipper;
        document.getElementById(Namespace.parameters).src = "partials/clipping.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим отсечения";
    },

    setEquationsMode: function () {
        this.ctrl = equationDrawCtrl;
        this.drawer = equationDrawer;
        document.getElementById(Namespace.parameters).src = "partials/equations.html";
        document.getElementById(Namespace.modeInfo).innerHTML = "Режим решения уравнений";
    },

    setParametersDoc: function (doc) {
        this.parametersDoc = doc;
    },

    selectController: function (newCtrl) {
        this.ctrl = newCtrl;
    },

    selectDrawer: function (newDrawer) {
        this.drawer = newDrawer;
    },

    startDebug: function () {
        document.getElementById(Namespace.debugButton).disabled = true;
        document.getElementById(Namespace.nextStepButton).style.display = "inline";
        document.getElementById(Namespace.stopButton).style.display = "inline";
    },

    stopDebug: function () {
        delete this.ctrl.debug;
        document.getElementById(Namespace.debugButton).disabled = false;
        document.getElementById(Namespace.nextStepButton).style.display = "none";
        document.getElementById(Namespace.stopButton).style.display = "none";
    },

    nextStep: function () {
        if (this.ctrl.debug.hasNext()) {
            this.ctrl.debug.nextStep();
            WorkPlace.repaint();
        }
        else {
            this.stopDebug();
        }
    }
};

document.addEventListener("DOMContentLoaded", function () {
        var LEFT_MOUSE_BUTTON = 1;
        var RIGHT_MOUSE_BUTTON = 3;
        var canvas = document.getElementById(Namespace.pixelsCanvas);
        var isMove = false;
        var x;
        var y;
        var currentPixel;

        canvas.addEventListener("mouseup", function (event) {

            if (isMove == false) {
                var physPoint = new Point(this.x, this.y);
                var logPoint = WorkPlace.translate(physPoint);

                switch (event.which) {
                    case LEFT_MOUSE_BUTTON:
                        app.ctrl.setPoint1(logPoint.x, logPoint.y);
                        break;
                    case RIGHT_MOUSE_BUTTON:
                        app.ctrl.setPoint2(logPoint.x, logPoint.y);
                        break;
                }
                previousColor = WorkPlace.getPixelColor(logPoint.x, logPoint.y);
                WorkPlace.drawOnePixel(logPoint.x, logPoint.y, Colors.pink);

            }
            else
                isMove = false;

            //WorkPlace.repaint();

        }, false);


        canvas.addEventListener("mousemove", function (event) {
                switch (event.which) {
                    case LEFT_MOUSE_BUTTON:
                        var dx = getMouseX(event),
                            dy = getMouseY(event),
                            physPoint = new Point(this.x, this.y),
                            dphysPoint = new Point(dx, dy),
                            logPoint = WorkPlace.translate(physPoint),
                            dlogPoint = WorkPlace.translate(dphysPoint);
                        previousColor = "#FFFFFF";

                        isMove = app.ctrl.movePoint1(logPoint, dlogPoint);

                        break;
                    case 0:
                        var physPoint = new Point(getMouseX(event), getMouseY(event)),
                            point = WorkPlace.translate(physPoint);
                        if (this.currentPixel == null) {
                            this.currentPixel = point;
                            previousColor = WorkPlace.getPixelColor(point.x, point.y);
                        }
                        else if (!equalPoints(this.currentPixel, point)) {
                            WorkPlace.drawOnePixel(this.currentPixel.x, this.currentPixel.y, previousColor);
                            previousColor = WorkPlace.getPixelColor(point.x, point.y);
                            this.currentPixel = point;
                        }


                        WorkPlace.drawOnePixel(point.x, point.y, Colors.pink);
                        //WorkPlace.repaint();
                        break;
                }
            }, false
        );

        canvas.addEventListener("mousedown", function (event) {
            this.x = getMouseX(event);
            this.y = getMouseY(event);
        }, false);


        function getMouseX(event) {
            return event.offsetX - canvas.offsetLeft;
        }

        function getMouseY(event) {
            return event.offsetY - canvas.offsetTop - 3;
        }


//	canvas.addEventListener("mouseup", function (event) {
//        var x = event.offsetX - canvas.offsetLeft;
//        var y = event.offsetY - canvas.offsetTop;
//        var physPoint = new Point(x, y);
//        var logPoint = WorkPlace.translate(physPoint);
//		WorkPlace.drawPixel(logPoint.x, logPoint.y, previousColor);
//		WorkPlace.repaint();
//    }, false);

        canvas.addEventListener('contextmenu', function (event) {
            event.preventDefault();
        }, false);
        /*
         canvas.addEventListener("keydown", function (event) {
         console.log(event);
         }, false);
         */
        WorkPlace.create(100, 100);
        WorkPlace.repaint();

    },
    false
);

document.addEventListener("keydown", function () {
    var KEY_LEFT = 37;
    var KEY_UP = 38;
    var KEY_RIGHT = 39;
    var KEY_DOWN = 40;

    if (window.event.ctrlKey) {
        if (window.event.keyCode == KEY_LEFT) {
            // rotate left
            app.ctrl.rotate(0, 2, 0);
        }
        else if (window.event.keyCode == KEY_UP) {
            // rotate up
            app.ctrl.rotate(-2, 0, 0);
        }
        else if (window.event.keyCode == KEY_RIGHT) {
            // rotate right
            app.ctrl.rotate(0, -2, 0);
        }
        else if (window.event.keyCode == KEY_DOWN) {
            // rotate down
            app.ctrl.rotate(2, 0, 0);
        }
    } else if (window.event.altKey) {
        if (window.event.keyCode == KEY_UP) {
            // scale +
            app.ctrl.scale(1.1, 1.1, 1.1);
        }
        else if (window.event.keyCode == KEY_DOWN) {
            // scale -
            app.ctrl.scale(0.9, 0.9, 0.9);
        }
    } else if (window.event.shiftKey) {
        if (window.event.keyCode == KEY_LEFT) {
            // translate left
            app.ctrl.translate(-1, 0, 0);
        }
        else if (window.event.keyCode == KEY_UP) {
            // translate up
            app.ctrl.translate(0, -1, 0);
        }
        else if (window.event.keyCode == KEY_RIGHT) {
            // translate right
            app.ctrl.translate(1, 0, 0);
        }
        else if (window.event.keyCode == KEY_DOWN) {
            // translate down
            app.ctrl.translate(0, 1, 0);
        }
    } else {
        return;
    }

    app.ctrl.redraw();

}, false);