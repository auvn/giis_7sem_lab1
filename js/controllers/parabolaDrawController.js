﻿var parabolaDrawCtrl = {

    points: [],

    paramP: 0,

    draw: function (debug) {

        var p = parseFloat(app.parametersDoc.getElementById("p").value);

        this.paramP = p;


        if (debug) {
            this.debug = app.drawer.parabolaDebug(this.points[0], p);
            app.startDebug();
        }
        else {
            app.drawer.parabolaDraw(app.drawer.parabola(this.points[0], p));

            if (this.debug) {
                app.stopDebug();
            }
        }

        WorkPlace.repaint();
    },

    translate: function (dx, dy, dz) {
        this.points[0] = this.points[0].translate(dx, dy, dz);
    },

    // set center point
    setPoint1: function (x, y) {
        this.points[0] = new Point3D(x, y, 0);
    },

    setPoint2: function (x, y) {
        console.log("undefined operation");
    },

    redraw: function () {
        this.clear();
        app.drawer.parabola(this.points[0], this.paramP);
        WorkPlace.repaint();
    },

    clear: function () {
        baseDrawer.clear();

    }
};

