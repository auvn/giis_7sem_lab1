function EdgeTr(point1, point2) {
    this.org = point1;
    this.dest = point2;

    this.flip = function () {
        this.rot();
        this.rot();
    };

    //returns t param for line through two points

    this.intersect = function (edge, param) {
        var a = this.org, b = this.dest, c = edge.org, d = edge.dest;
        var n = new Point((d.minus(c)).y, c.minus(d).x);
        var den = this.dotProduct(n, b.minus(a));
        if (den == 0)
            return param;
        var num = this.dotProduct(n, a.minus(c));
        param = (-num) / den;
        return param;

    };
    //scalar multiply

    this.dotProduct = function (point1, point2) {
        return (point1.x * point2.x + point1.y * point2.y);
    }
    //rotate on 90 degrees

    this.rot = function () {
        var m = (this.org.plus(this.dest)).multiply(0.5);
        var v = this.dest.minus(this.org);
        var n = new Point(v.y, -v.x);
        this.org = m.minus(n.multiply(0.5));
        this.dest = m.plus(n.multiply(0.5));
    };
}

var EdgeDictionary = {
    edges: [],

    //finds edge in the edges list
    find: function (edge) {
        for (var i = 0; i < this.edges.length; i++) {
            var curEdge = this.edges[i];
            if (curEdge.org.eq(edge.org) && curEdge.dest.eq(edge.dest))
                return true;
        }
        return false;
    },

    //removing edge from edges

    remove: function (edge) {
        for (var i = 0; i < this.edges.length; i++) {
            var curEdge = this.edges[i];
            if (curEdge.org.eq(edge.org) && curEdge.dest.eq(edge.dest)) {
                this.edges.splice(i, 1);
                break;
            }
        }
    },
    insert: function (edge) {
        if (!this.find(edge))
            this.edges.push(edge);
    },

    //edit dictionary after conjugate point has been found for edge
    update: function (a, b) {
        var edge = new EdgeTr(a, b);
        if (this.find(edge))
            this.remove(edge);
        else {
            edge.flip();
            this.insert(edge);
        }
    },

    isEmpty: function () {
        return this.edges.length == 0 ? true : false;
    },

    removeMin: function () {
        var edge = this.edges[0];
        this.edges.shift();
        return edge;
    }
}

var DeloneTriangulation = {


    triangulate: function (points) {
        var result = [];
        //edges
        var dict = EdgeDictionary;

        //first iteration of method Djarvis
        var edge = this.hullEdge(points);

        dict.insert(edge);

        while (!dict.isEmpty()) {
            //getting next edge
            edge = dict.removeMin();
            //finding conjugate point
            var point = this.mate(edge, points);

            if (point != null) {
                //if found then point creates triangle with current edge
                //current edge going to death mode
                //and two new has to going to live mode
                dict.update(point, edge.org);
                dict.update(edge.dest, point);
                result.push([edge.org, edge.dest, point]);
            }
        }
        return result;
    },

    hullEdge: function (points) {

        var len = points.length;

        var m = 0;
        //left lower point
        for (var i = 1; i < len; i++) {
            if (points[i].lessThan(points[m]))
                m = i;
        }
        var temp = points[0];
        points[0] = points[m];
        points[m] = temp;
        //calculate position of points regardign two another
        var i = 0;
        for (var m = 1, i = 2; i < len; i++) {
            var c = points[i].classify(points[0], points[m]);
            if (c == 0 || c == 6)  //lef or between
                m = i;
        }
        var edge = new EdgeTr(points[0], points[m]);

        return edge;

    },
    //returns conjugate point
    mate: function (edge, points) {
        var bestPoint;
        var len = points.length;

        var t = 999999;
        var bestt = 999999;

        var edge1 = new EdgeTr(edge.org, edge.dest);
        edge1.rot();
        //edge rotated on 90 degrees regarding its center .
        //center  of circumcircle lay on intersection perpendiculars,
        //drawed through center of lines

        for (var i = 0; i < len; i++) {
            if (points[i].classifyByEdge(edge) == 1) {
                var edge2 = new EdgeTr(edge.dest, points[i])
                edge2.rot();
                t = edge1.intersect(edge2, t);
                if (t < bestt) {
                    bestPoint = points[i];
                    bestt = t;
                }
            }
        }
        return bestPoint;

    }
}

var trianglesDrawCtrl = {
    points: [ ],

    triangulationObject: DeloneTriangulation,

    draw: function (debug) {
        var triangles = this.triangulationObject.triangulate(this.points);
        app.drawer.triangles(triangles);
        baseDrawer.plotPoints(this.points, Colors.pink);
        WorkPlace.repaint();
    },

    setPoint1: function (x, y) {
        var point = new Point(x, y);

        this.points.push(point);
        app.parametersDoc.getElementById("pointsCount").innerHTML = this.points.length;
    },

    clearPoints: function () {
        delete this.points;
        this.points = [];
    },

    clear: function () {
        this.clearPoints();
        baseDrawer.clear();
    }
}



