var baseDrawer = {

    backgroundColor: Colors.white,
    baseDrawColor: Colors.black,

    plotPoints: function (points, color) {
        for (var i = 0; i < points.length; i++) {
            var point = points[i];
            WorkPlace.putPixel(point.x, point.y, color);
        }

    },

    plot: function (x, y) {
		var xint = parseInt(x);
		var yint = parseInt(y);
        WorkPlace.putPixel(x, y, this.baseDrawColor);
//        console.log("plotting the point x=" + x + ", y=" + y);
    },

    plotCompl: function (x, y, brightness) {
        var partColor = 255 - Math.round(255 * brightness);
        var hexPartColor = partColor.toString(16);
        var color = "#" + hexPartColor + hexPartColor + hexPartColor;
        WorkPlace.putPixel(x, y, color);
        console.log("plotting the point x=" + x + ", y=" + y + " with b=" + Math.round(brightness) + " color=" + color);
    },

    zoom: function (newScale) {
        WorkPlace.changeScale(WorkPlace.scale + newScale);
    },

    clear: function () {
        //govnokod
        WorkPlace.create(100, 100);
        WorkPlace.repaint();
    },

    buildAxis: function () {
        var xhalf = Math.ceil(WorkPlace.width / 2);
        var yhalf = Math.ceil(WorkPlace.height / 2);
        for (var x = 0; x <= WorkPlace.width; x++) {
            WorkPlace.putPixel(x, yhalf, "#40007f");
        }
        for (var y = 0; y <= WorkPlace.height; y++) {
            WorkPlace.putPixel(xhalf, y, "#40007f");
        }

        WorkPlace.repaint();
    }


}