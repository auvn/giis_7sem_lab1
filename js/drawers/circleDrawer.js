﻿var circleDrawer = {



    circle: function (c, r) {

        var points = [];

        var x = -r;
        var y = 0;
        var F = 1 - r;
        var dFs = 3;
        var dFd = 5 - 2 * r;
        while (x + y < 0) {
            points.push(new Point(x + c.x, y + c.y));		// 4 октант
            points.push(new Point(-x + c.x, y + c.y));		// 1 октант
            points.push(new Point(x + c.x, -y + c.y));		// 5 октант
            points.push(new Point(-x + c.x, -y + c.y));	// 8 октант

            points.push(new Point(y + c.x, x + c.y));		// 7 октант
            points.push(new Point(-y + c.x, x + c.y));		// 6 октант
            points.push(new Point(y + c.x, -x + c.y));		// 2 октант
            points.push(new Point(-y + c.x, -x + c.y));	// 3 октант

            if (F > 0) {
                F += dFd;
                x++;
                y++;
                dFs += 2;
                dFd += 4;
            }
            else {
                F += dFs;
                y++;
                dFs += 2;
                dFd += 2;
            }
        }
        if (x + y == 0) {
            points.push(new Point(x + c.x, y + c.y));		// 4 октант
            points.push(new Point(-x + c.x, y + c.y));		// 1 октант
            points.push(new Point(x + c.x, -y + c.y));		// 5 октант
            points.push(new Point(-x + c.x, -y + c.y));	// 8 октант
        }

        return points;

    },

    circleDraw: function (points) {
        for (var i = 0; i < points.length; i++) {
            baseDrawer.plot(points[i].x,points[i].y);
        }
    },

    circleDebug: function (c, r) {
        var state = {};
        state.x = -r;
        state.y = 0;
        state.F = 1 - r;
        state.dFs = 3;
        state.dFd = 5 - 2 * r;

        state.step = 0;
        state.nextStep = function () {
            var x = this.x;
            var y = this.y;
            if (x + y < 0) {
                switch (this.step) {
                    case 0:
                        baseDrawer.plot(y + c.x, x + c.y);		// 7 октант
                        break;
                    case 1:
                        baseDrawer.plot(-x + c.x, -y + c.y);	// 8 октант
                        break;
                    case 2:
                        baseDrawer.plot(-x + c.x, y + c.y);		// 1 октант
                        break;
                    case 3:
                        baseDrawer.plot(y + c.x, -x + c.y);		// 2 октант
                        break;
                    case 4:
                        baseDrawer.plot(-y + c.x, -x + c.y);	// 3 октант
                        break;
                    case 5:
                        baseDrawer.plot(x + c.x, y + c.y);		// 4 октант
                        break;
                    case 6:
                        baseDrawer.plot(x + c.x, -y + c.y);		// 5 октант
                        break;
                    case 7:
                        baseDrawer.plot(-y + c.x, x + c.y);		// 6 октант
                        if (this.F > 0) {
                            this.F += this.dFd;
                            this.x++;
                            this.y++;
                            this.dFs += 2;
                            this.dFd += 4;
                        }
                        else {
                            this.F += this.dFs;
                            this.y++;
                            this.dFs += 2;
                            this.dFd += 2;
                        }
                        this.step = -1;
                }
                this.step++;
            }
        };
        state.hasNext = function () {
            return this.x + this.y < 0;
        };
        return state;
    }
};