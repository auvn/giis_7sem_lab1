var LEFT = 1;	/* двоичное 0001 */
var RIGHT = 2;	/* двоичное 0010 */
var BOT = 4;	/* двоичное 0100 */
var TOP = 8;	/* двоичное 1000 */

function rect(x_min, y_min, x_max, y_max) {
	this.x_min = x_min;
	this.y_min = y_min;
	this.x_max = x_max;
	this.y_max = y_max;
}

var cohSuthClipper = {

	/* если отрезок ab не пересекает прямоугольник r, функция возвращает -1;
	 * если отрезок ab пересекает прямоугольник r, функция возвращает 0 и отсекает
	 * те части отрезка, которые находятся вне прямоугольника
	 */
	clip: function(r, a, b) {
	
		/* вычисление кода точки
		 * r : указатель на struct rect;
		 * p : указатель на struct point
		 */
		function vcode(r, p) {
			return	p.x < r.x_min ? LEFT : 0 +		/* +1 если точка левее прямоугольника */
					p.x > r.x_max ? RIGHT : 0 +		/* +2 если точка правее прямоугольника */
					p.y < r.y_min ? BOT : 0 +		/* +4 если точка ниже прямоугольника */
					p.y > r.y_max ? TOP : 0;		/* +8 если точка выше прямоугольника */
		}
	
		var code_a, code_b, code;	/* код конечных точек отрезка */
        var c;						/* одна из точек (Point) */
 
        code_a = vcode(r, a);
        code_b = vcode(r, b);
 
        /* пока одна из точек отрезка вне прямоугольника */
        while (code_a | code_b) {
                /* если обе точки с одной стороны прямоугольника, то отрезок не пересекает прямоугольник */
                if (code_a & code_b)
                        return -1;
 
                /* выбираем точку c с ненулевым кодом */
                if (code_a) {
                        code = code_a;
                        c = a;
                } else {
                        code = code_b;
                        c = b;
                }
 
                /* если c левее r, то передвигаем c на прямую x = r.x_min
                   если c правее r, то передвигаем c на прямую x = r.x_max */
                if (code & LEFT) {
                        c.y += (a.y - b.y) * (r.x_min - c.x) / (a.x - b.x);
                        c.x = r.x_min;
                } else if (code & RIGHT) {
                        c.y += (a.y - b.y) * (r.x_max - c.x) / (a.x - b.x);
                        c.x = r.x_max;
                }/* если c ниже r, то передвигаем c на прямую y = r.y_min
                    если c выше r, то передвигаем c на прямую y = r.y_max */
                else if (code & BOT) {
                        c.x += (a.x - b.x) * (r.y_min - c.y) / (a.y - b.y);
                        c.y = r.y_min;
                } else if (code & TOP) {
                        c.x += (a.x - b.x) * (r.y_max - c.y) / (a.y - b.y);
                        c.y = r.y_max;
                }
 
                /* обновляем код */
                if (code == code_a) {
                        a = c;
                        code_a = vcode(r,a);
                } else {
                        b = c;
                        code_b = vcode(r,b);
                }
        }
 
        /* оба кода равны 0, следовательно обе точки в прямоугольнике */
        return 0;
	}	
};


var cyrusBeckClipper = {

	// compute the outer normals.  
	// note that this requires that the polygon be convex
	// to always work
	// in_:  p - Polygon 	(Point[])
	// out_: n - Normal		(Point[])
	calcNormals: function (p, n) {		
		for (var i = 0; i < p.length; i++) {
			var j = (i+1) % p.length;
			var k = (i+2) % p.length;
			// make vector be -1/mI + 1J
			n[i] = new Point();
			n[i].x = -(p[j].y - p[i].y) / (p[j].x - p[i].x);
			n[i].y = 1.0;
			var vx = p[k].x - p[i].x;
			var vy = p[k].y - p[i].y;
			var v = new Point(vx, vy);
			if (this.dotProduct(n[i],v) > 0) {
				// inner normal
				n[i].x *= -1;
				n[i].y *= -1;
			}
		}
	},
	
	// input - two points
	dotProduct: function (v1, v2) {
		return v1.x * v2.x + v1.y * v2.y;
	},
	
	clip: function (p, p1, p2, rp, q) {
		var n = [];
		this.calcNormals(p, n);
		
		var t;
		var dirV = new Point();
		var F = new Point();

		// start largest at smallest legal value and smallest 
		// at largest legal value
		var t1 = 0.0;
		var t2 = 1.0;

		// compute the direction vector
		dirV.x = p2.x - p1.x;
		dirV.y = p2.y - p1.y;
		var visible = true;
		var i = 0;
		while ((i < p.length) && visible) {
			F.x = p1.x - p[i].x;
			F.y = p1.y - p[i].y;
			var num = this.dotProduct(n[i], F);
			var den = this.dotProduct(n[i], dirV);

			// Parallel or Point
			if (den == 0.0) {
				// parallel - if outside then forget the line; if inside then there are no 
				// intersections with this side 
				// but there may be with other edges, so in this case just keep going
				if (num > 0.0) {
					//   Parallel and outside or point (p1 == p2) and outside
					visible = false;
				}
			}
			else {
				t = -(num/den);
				if (den < 0.0) {
					// entering
					if (t <= 1.0 && t > t1) {
						t1 = t;
					}
				}
				else if (t >= 0.0) {
					//exiting
					if (t < t2) {
						t2 = t;
					}
				}
			}
			i++;
		}
		if (t1 <= t2) {
			rp.x = p1.x + t1 * dirV.x;
			rp.y = p1.y + t1 * dirV.y;
			q.x = p1.x + t2 * dirV.x;
			q.y = p1.y + t2 * dirV.y;
		}
		else {
			visible = false;
		}
		return visible - 1;
	}
};