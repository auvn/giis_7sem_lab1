﻿var cubeDrawer = {

    cube: function (points, x, y, z, f, removeUnvisible) {

        var p0 = points[0].projection(x, y, z, f);
        var p1 = points[1].projection(x, y, z, f);
        var p2 = points[2].projection(x, y, z, f);
        var p3 = points[3].projection(x, y, z, f);
        var p4 = points[4].projection(x, y, z, f);
        var p5 = points[5].projection(x, y, z, f);
        var p6 = points[6].projection(x, y, z, f);
        var p7 = points[7].projection(x, y, z, f);


        var cubeRelatedPoint = p3.plus(p5).devide(2);

        var side1 = new Side(PlaneEquation.getPlaneMatrix(p0, p1, p2)); //front
        var side2 = new Side(PlaneEquation.getPlaneMatrix(p4, p5, p6)); //behind
        var side4 = new Side(PlaneEquation.getPlaneMatrix(p0, p4, p5)); //top
        var side3 = new Side(PlaneEquation.getPlaneMatrix(p2, p6, p7)); //bottom
        var side5 = new Side(PlaneEquation.getPlaneMatrix(p1, p5, p6)); //right
        var side6 = new Side(PlaneEquation.getPlaneMatrix(p0, p4, p7)); //left

        side1.addPoint(p0);
        side1.addPoint(p1);
        side1.addPoint(p2);
        side1.addPoint(p3);

        side2.addPoint(p4);
        side2.addPoint(p5);
        side2.addPoint(p6);
        side2.addPoint(p7);

        side3.addPoint(p3);
        side3.addPoint(p2);
        side3.addPoint(p6);
        side3.addPoint(p7);

        side4.addPoint(p0);
        side4.addPoint(p1);
        side4.addPoint(p5);
        side4.addPoint(p4);

        side5.addPoint(p1);
        side5.addPoint(p5);
        side5.addPoint(p6);
        side5.addPoint(p2);

        side6.addPoint(p0);
        side6.addPoint(p4);
        side6.addPoint(p7);
        side6.addPoint(p3);


        var sides = [];
        sides.push(side1);
        sides.push(side2);
        sides.push(side3);
        sides.push(side4);
        sides.push(side5);
        sides.push(side6);
        if (removeUnvisible) {
            for (var i = 0; i < sides.length; i++) {
                sides[i].fixCoeff(cubeRelatedPoint);
            }


            algoRobertson.setVisibility(sides, new Point3D(35, 35, -10000, 1));
        } else {
            for (var i = 0; i < sides.length; i++) {
                sides[i].paint(brezLineDrawer);
            }
        }
    }

};

var algoRobertson = {
    run: function (sides, points, edges) {
        var point = points[0];
        var gran = new Array(100);
        var scene = [];
        for (var i = 0; i < points.length; i++) {
            var tempPoint = points[i];
            scene[i] = tempPoint;
        }

        for (var i = 0; i < sides.length; i++) {
            var side = sides[i];
            var temp = new Point3D(side.A, side.B, side.C, side.D);
            if (scalarMultiply(temp, point) > 0) {
                var num = 0;
                for (var ti = 0; ti < points.length; ti++) {
                    if (side.posOfPointRelativePlane(scene[ti]) != 0) {
                        for (var tj = 0; tj < num; tj++)
                            if (gran[tj] == scene[ti])
                                break;
                        if (tj == num) {
                            gran[num] = scene[ti];
                            gran[num].i = ti;
                            num++;
                        }
                    }
                }

                for (var z = 1; z < num; z++)
                    for (tj = z; tj < num; tj++) {

                        if (this.isConnected(edges, scene[gran[z - 1].i], scene[gran[tj].i])) {
                            temp = gran[z];
                            gran[z] = gran[tj];
                            gran[tj] = temp;
                            break;
                        }
                    }

                for (var z = 0; z < num - 1; z++) {
                    brezLineDrawer.line(gran[z], gran[z + 1]);
                }

                brezLineDrawer.line(gran[0], gran[num - 1]);

            }
        }


    },

    isConnected: function (edges, point1, point2) {


        return (edges.find(new EdgeTr(point1, point2)) || edges.find(new EdgeTr(point2, point1)));
    },

    setVisibility: function (planes, pointOfView) {
        var planesMatrix = [];

        var coeff = ["A", "B", "C", "D"]

        for (var i = 0; i < coeff.length; i++) {
            var row = [];
            for (var j = 0; j < planes.length; j++) {
                row.push(planes[j][coeff[i]]);
            }
            planesMatrix.push(row);
        }


        var result = multiplyMatrix(pointOfView.getHorizontalMatrix(), planesMatrix)[0];

        for (var i = 0; i < result.length; i++) {
            if (result[i] > 0)
                planes[i].paint(brezLineDrawer);

        }

        return result;
    }

}