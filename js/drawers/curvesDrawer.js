var bezyeDrawer = {

    bazicMatrix: [
        [-1, 3, -3, 1],
        [3, -6, 3, 0],
        [-3, 3, 0, 0],
        [1, 0, 0, 0]
    ],

    curve: function (points, byMatrix) {
        if (points.length != 4) {
            alert("Error. You have to set 4 points");
        } else {

            var tStep = calculateTstepparam(points) / 3,

                point = points[0],
                p1x = point.x,
                p1y = point.y;

            point = points[1];
            p2x = point.x;
            p2y = point.y;

            point = points[2];
            p3x = point.x;
            p3y = point.y;

            point = points[3];
            p4x = point.x;
            p4y = point.y;

            for (var t = 0.0; t <= 1.0; t += tStep) {
                baseDrawer.plot(Math.round(this.calculatePoint(t, p1x, p2x, p3x, p4x)), Math.round(this.calculatePoint(t, p1y, p2y, p3y, p4y)));
            }
        }
    },

    calculatePoint: function (t, p1, p2, p3, p4) {
        var firstPart = multiplyMatrix(getTmatrix(t), this.bazicMatrix);
        return multiplyMatrix(firstPart, getPMatrix(p1, p2, p3, p4));
    },

    calculatePointByFormula: function (t, p1, p2, p3, p4) {
        return (Math.pow(1 - t, 3)) * p1 + 3 * t * (Math.pow(t - 1, 2)) * p2 + 3 * (Math.pow(t, 2)) * (1 - t) * p3 + (Math.pow(t, 3)) * p4;

    }
}


var ermitDrawer = {
    bazicMatrix: [
        [2, -2, 1, 1],
        [-3, 3, -2, -1],
        [0, 0, 1, 0],
        [1, 0, 0, 0]
    ],

    curve: function (points, byMatrix) {
        if (points.length != 4) {
            alert("Error. You have to set 4 points");
        } else {

            var tStep = calculateTstepparam(points) / 3,

                p1 = points[0],
                p2 = points[1],
                p3 = points[2],
                p4 = points[3],

                r1 = calculateRVector(p1, p2),
                r4 = calculateRVector(p3, p4);


            for (var t = 0.0; t <= 1.0; t += tStep) {
                baseDrawer.plot(Math.round(this.calculatePoint(t, p1.x, p4.x, r1.x, r4.x)), Math.round(this.calculatePoint(t, p1.y, p4.y, r1.y, r4.y)));
            }
        }
    },
    calculatePoint: function (t, p1, p4, r1, r4) {
        var firstPart = multiplyMatrix(getTmatrix(t), this.bazicMatrix);
        return multiplyMatrix(firstPart, getPMatrix(p1, p4, r1, r4));

    },

    calculateByFormula: function (t, p1, p4, r1, r4) {
        return p1 * (2 * Math.pow(t, 3) - 3 * Math.pow(t, 2) + 1) + p4 * (-2 * Math.pow(t, 3) + 3 * Math.pow(t, 2)) + r1 * (Math.pow(t, 3) - 2 * Math.pow(t, 2) + t) + r4 * (Math.pow(t, 3) - Math.pow(t, 2));
    }

}

var bSplinesDrawer = {
    bazicMatrix: [
        [-1, 3, -3, 1],
        [3, -6, 3, 0],
        [-3, 0, 3, 0],
        [1, 4, 1, 0]
    ],
    curve: function (points, byMatrix) {
        var tStep = calculateTstepparam(points);


        for (var i = 0; i < points.length; i++) {

            var prevIndex = this.getIndex(points, i - 1);

            var nextIndex = this.getIndex(points, i + 1);
            var nextIndex2 = this.getIndex(points, i + 2);

            var p1x = points[prevIndex].x,
                p1y = points[prevIndex].y,
                p2x = points[i].x,
                p2y = points[i].y,
                p3x = points[nextIndex].x,
                p3y = points[nextIndex].y,
                p4x = points[nextIndex2].x,
                p4y = points[nextIndex2].y;


            for (var t = 0.0; t <= 1.0; t += tStep) {
                baseDrawer.plot(Math.round(this.calculatePoint(t, p1x, p2x, p3x, p4x)), Math.round(this.calculatePoint(t, p1y, p2y, p3y, p4y)));
            }
        }
    },

    getIndex: function (array, index) {
        var len = array.length;

        if (index == -1)
            return len - 1;
        else if (index == len)
            return 0;
        else if (index > len)
            return index - len;

        return index;

    },

    calculatePoint: function (t, p1, p2, p3, p4) {
        var firstPart = multiplyMatrix(getTmatrix(t), this.bazicMatrix);
        return  multiplyMatrix(firstPart, getPMatrix(p1, p2, p3, p4)) * (1 / 6.);
    }

}


function calculateRVector(p1, p2) {
    return new Point(p2.x - p1.x, p2.y - p1.y);
}

function getPMatrix(p1, p2, p3, p4) {
    return [
        [p1],
        [p2],
        [p3],
        [p4]
    ];
}

function getTmatrix(t) {
    return [
        [Math.pow(t, 3),
            Math.pow(t, 2),
            t,
            1]
    ];
}

function calculateTstepparam(points) {
    var tStep = 0.1,

        maxLength = 0,
        i = 0;

    for (i = 0; i < points.length; i++) {

        if (i + 1 != points.length) {
            var point1 = points[i],
                point2 = points[i + 1];

            maxLength = Math.max(Math.abs(point2.x - point1.x), Math.abs(point2.y - point1.y), maxLength);
        }
    }

    tStep = 1. / maxLength;
    return tStep;
}

