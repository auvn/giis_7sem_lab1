var equationDrawer = {

    drawAndCalculate: function (points1, points2) {
        var commonPoints = [];


        this.draw(points1);
        this.draw(points2);

        for (var i = 0; i < points1.length; i++) {
            var point1 = points1[i]
            for (var j = 0; j < points2.length; j++) {
                var point2 = points2[j];
                if (point1.eq(point2))
                    commonPoints.push(point1);
//                else if (new Point(point2.x + 1, point2.y).eq(point1))
//                    commonPoints.push(point1);
                else if (new Point(point2.x - 1, point2.y).eq(point1))
                    commonPoints.push(point1);
//                else if (new Point(point2.x, point2.y + 1).eq(point1))
//                    commonPoints.push(point1);
                else if (new Point(point2.x, point2.y + 1).eq(point1))
                    commonPoints.push(point1);
            }
        }
        return commonPoints;


    },

    draw: function (points) {
        for (var i = 0; i < points.length; i++) {
            baseDrawer.plot(points[i].x, points[i].y);
        }
    }

}
