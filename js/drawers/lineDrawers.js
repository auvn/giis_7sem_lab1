var cdaLineDrawer = {

    line: function (p1, p2) {
        var points = [];
        // ����� ���������� �������� �������
        var length = Math.max(Math.abs(p2.x - p1.x), Math.abs(p2.y - p1.y));

        // ���������� �� ���� X � Y
        var dx = (p2.x - p1.x) / length;
        var dy = (p2.y - p1.y) / length;

        // ��������� ����� (�, �)
        var x = p1.x + 0.5;
        var y = p1.y + 0.5;

        // ���������� ��������� �����
        for (var i = 0; i <= length; i++) {
            // ��������� �� �������� ������
            var xint = Math.floor(x);
            var yint = Math.floor(y);
            // ������ ����� � ������������ (xint, yint)
            baseDrawer.plot(xint, yint);
            // ��������� ����������
            x += dx;
            y += dy;
        }
        return points;
    },

    lineDraw: function (points) {
        for (var i = 0; i < points.length; i++) {
            baseDrawer.plot(points[i].x, points[i].y);
        }
    },

    lineDebug: function (p1, p2) {
        var state = {};
        state.length = Math.max(Math.abs(p2.x - p1.x), Math.abs(p2.y - p1.y));
        state.dx = (p2.x - p1.x) / state.length;
        state.dy = (p2.y - p1.y) / state.length;
        state.x = p1.x;
        state.y = p1.y;
        state.step = 0;
        state.nextStep = function () {
            var xint = parseInt(this.x);
            var yint = parseInt(this.y);
            baseDrawer.plot(xint, yint);
            this.x += this.dx;
            this.y += this.dy;
            this.step++;
        };
        state.hasNext = function () {
            return this.step <= this.length;
        };
        return state;
    }
};

var brezLineDrawer = {

    line: function (p1, p2) {
        var points = [];
        // �������� �� ��� X � Y
        var deltaX = p2.x - p1.x;
        var deltaY = p2.y - p1.y;

        // ����� �������� �� ��� X � Y
        var dx = Math.abs(deltaX);
        var dy = Math.abs(deltaY);

        // �������� �������� �������, ����������� ������� ������� �� ���� ����������
        var signX = sign(deltaX);
        var signY = sign(deltaY);

        // �������� ������
        var e = dx - dy;

        // ��������� �����
        var x = p1.x;
        var y = p1.y;

        // ���������� ��������� �����
        while (x != p2.x || y != p2.y) {
            points.push(new Point(x, y));

            // ��������� ������, ����������� �������� � ������ �������
            var e2 = 2 * e;
            if (e2 > -dx) {
                e -= dy;
                x += signX;
            }
            if (e2 < dx) {
                e += dx;
                y += signY;
            }
        }
        points.push(new Point(p2.x, p2.y));
        return points;
    },

    lineDraw: function (points) {
        for (var i = 0; i < points.length; i++) {
            baseDrawer.plot(points[i].x, points[i].y);
        }
    },

    lineDebug: function (p1, p2) {
        var state = {p1: p1, p2: p2};

        var deltaX = p2.x - p1.x;
        var deltaY = p2.y - p1.y;
        state.dx = Math.abs(deltaX);
        state.dy = Math.abs(deltaY);
        state.signX = sign(deltaX);
        state.signY = sign(deltaY);
        state.e = state.dx - state.dy;
        state.x = p1.x;
        state.y = p1.y;
        state.last = false;

        console.log("dy=" + state.dy);
        state.nextStep = function () {
            baseDrawer.plot(this.x, this.y);
            var e2 = 2 * this.e;
            console.log("e2 = " + e2);
            console.log("dx=" + state.dx);
            if (e2 > -this.dx) {
                this.e -= this.dy;
                this.x += this.signX;
                console.log("make step over x");
            }
            if (e2 < this.dx) {
                this.e += this.dx;
                this.y += this.signY;
                console.log("make step over y");
            }
        };
        state.hasNext = function () {
            if (this.last) {
                return false;
            }
            if (this.x == this.p2.x && this.y == this.p2.y) {
                this.last = true;
            }
            return true;
        };
        return state;
    }
};

var wuLineDrawer = {

    line: function (p1, p2) {

        // ��������
        var dx = p2.x - p1.x;
        var dy = p2.y - p1.y;

        // ����� ��������
        var deltaX = Math.abs(dx);
        var deltaY = Math.abs(dy);

        // ���� ������ ��������������, ������������ ��� ������������ �����,
        // �� ���������� �������� ����������
        if (p1.x == p2.x || p1.y == p2.y || deltaX == deltaY) {
            brezLineDrawer.line(p1, p2);
            return;
        }

        // ����, ����������� �������������� �������� ��� ������ �� ���� ����������
        var steep = deltaY > deltaX;

        // ���������� ��� ������ ����������
        var temp;

        // ���� ����� �������� �� ��� Y ������ ����� �������� �� ��� X,
        // �� ������ �������� x � y � ����� �����
        if (steep) {
            temp = p1.x;
            p1.x = p1.y;
            p1.y = temp;

            temp = p2.x;
            p2.x = p2.y;
            p2.y = temp;
        }

        // ���� ������ ������� � ������� ������ �������� (������������� ����������),
        // �� ������ �������� ���������� ��������� ����� ����� �������,
        // ����� ���������� ����������� c ������������� �����������
        if (p1.x > p2.x) {
            temp = p1.x;
            p1.x = p2.x;
            p2.x = temp;

            temp = p1.y;
            p1.y = p2.y;
            p2.y = temp;
        }

        // ����������
        var grad = steep ? dx / dy : dy / dx;

        // ��������� �����
        var xend = Math.round(p1.x);
        var yend = p1.y + grad * (xend - p1.x);
        var xgap = rfpart(p1.x + 0.5);
        var xpxl1 = xend;
        var ypxl1 = Math.floor(yend);

        if (steep) {
            baseDrawer.plotCompl(ypxl1, xpxl1, rfpart(yend) * xgap);
            baseDrawer.plotCompl(ypxl1 + 1, xpxl1, fpart(yend) * xgap);
        }
        else {
            baseDrawer.plotCompl(xpxl1, ypxl1, rfpart(yend) * xgap);
            baseDrawer.plotCompl(xpxl1, ypxl1 + 1, fpart(yend) * xgap);
        }

        // ������ ����������� � y ��� �������� �����
        var intery = yend + grad;

        // �������� �����
        xend = Math.round(p2.x);
        yend = p2.y + grad * (xend - p2.x);
        xgap = fpart(p2.x + 0.5);
        var xpxl2 = xend;
        var ypxl2 = Math.floor(yend);
        if (steep) {
            baseDrawer.plotCompl(ypxl2, xpxl2, rfpart(yend) * xgap);
            baseDrawer.plotCompl(ypxl2 + 1, xpxl2, fpart(yend) * xgap);
        }
        else {
            baseDrawer.plotCompl(xpxl2, ypxl2, rfpart(yend) * xgap);
            baseDrawer.plotCompl(xpxl2, ypxl2 + 1, fpart(yend) * xgap);
        }

        // ������� ���� (��������� �������� �����)
        for (var x = xpxl1 + 1; x <= xpxl2 - 1; x++) {
            if (steep) {
                baseDrawer.plotCompl(Math.floor(intery), x, rfpart(intery));
                baseDrawer.plotCompl(Math.floor(intery) + 1, x, fpart(intery));
            }
            else {
                baseDrawer.plotCompl(x, Math.floor(intery), rfpart(intery));
                baseDrawer.plotCompl(x, Math.floor(intery) + 1, fpart(intery));
            }
            intery += grad;
        }
    }
};