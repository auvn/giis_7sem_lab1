﻿var parabolaDrawer = {

    xmax: 100,

    parabola: function (c, p) {

        var points = []

        var x = 0;
        var y = 0;

        var Sd = ((y + 1) * (y + 1) ) - 2 * p * (x + 1);
        var Sv = ((y + 1) * (y + 1)) - 2 * p * x;
        var Sh = (y * y) - 2 * p * (x + 1);

        points.push(new Point(x + c.x, y + c.y)); // центр

        while (x + c.x < this.xmax) {
            if (Math.abs(Sh) <= Math.abs(Sv)) {
                if (Math.abs(Sd) < Math.abs(Sh)) {
                    y++;
                }
                x++;
            }
            else {
                if (Math.abs(Sv) > Math.abs(Sd)) {
                    x++;
                }
                y++;
            }

            points.push(new Point(x + c.x, y + c.y));
            points.push(new Point(x + c.x, -y + c.y));

            Sd = ((y + 1) * (y + 1)) - 2 * p * (x + 1);
            Sv = ((y + 1) * (y + 1)) - 2 * p * x;
            Sh = (y * y) - 2 * p * (x + 1);
        }
        return points;
    },

    parabolaDraw: function (points) {
        for (var i = 0; i < points.length; i++) {
            baseDrawer.plot(points[i].x, points[i].y);
        }
    },

    parabolaDebug: function (c, p) {
        var state = {};
        state.xmax = this.xmax;
        state.x = 0;
        state.y = 0;

        var x = 0;
        var y = 0;

        state.Sd = ((y + 1) * (y + 1) ) - 2 * p * (x + 1);
        state.Sv = ((y + 1) * (y + 1)) - 2 * p * x;
        state.Sh = (y * y) - 2 * p * (x + 1);

        state.step = 0;

        state.nextStep = function () {
            var x = this.x;
            var y = this.y;

            if (x + c.x < this.xmax) {
                switch (this.step) {
                    case 0:
                        baseDrawer.plot(x + c.x, y + c.y); // центр
                        this.step++;
                        break;
                    case 1:
                        if (Math.abs(this.Sh) <= Math.abs(this.Sv)) {
                            if (Math.abs(this.Sd) < Math.abs(this.Sh)) {
                                this.y++;
                            }
                            this.x++;
                        }
                        else {
                            if (Math.abs(this.Sv) > Math.abs(this.Sd)) {
                                this.x++;
                            }
                            this.y++;
                        }
                        baseDrawer.plot(this.x + c.x, this.y + c.y);
                        this.step++;
                        break;
                    case 2:
                        baseDrawer.plot(x + c.x, -y + c.y);
                        this.Sd = ((y + 1) * (y + 1)) - 2 * p * (x + 1);
                        this.Sv = ((y + 1) * (y + 1)) - 2 * p * x;
                        this.Sh = (y * y) - 2 * p * (x + 1);
                        this.step--;
                        break;
                }
            }
        };

        state.hasNext = function () {
            return this.x + c.x < this.xmax
        };
        return state;

    }

};