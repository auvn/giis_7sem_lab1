var trianglesDrawer = {

    lineDrawer: brezLineDrawer,

    triangles: function (triangles) {
        for (var i = 0; i < triangles.length; i++) {
            var triangle = triangles[i];

            var pointA = triangle[0];
            var pointB = triangle[1];
            var pointC = triangle[2];
            this.triangle(pointA, pointC, pointB);

        }
    },
    triangle: function (a, b, c) {
        this.lineDrawer.line(a, b);
        this.lineDrawer.line(a, c);
        this.lineDrawer.line(c, b);
    }

}