function Point(x, y) {
    this.x = x;
    this.y = y;
    this.z = 1;
    this.t = 1;
    this.plus = function (point) {
        return new Point(this.x + point.x, this.y + point.y);
    };

    this.minus = function (point) {
        return new Point(this.x - point.x, this.y - point.y);
    };

    this.classify = function (point1, point2) {
        var p2 = this;
        var a = point2.minus(point1);
        var b = p2.minus(point1);

        var sa = a.x * b.y - b.x * a.y;
        //left
        if (sa > 0)
            return 0;
        //right
        if (sa < 0)
            return 1;
        //behind
        if ((a.x * b.x < 0) || (a.y * b.y < 0))
            return 2;
        //beyond
        if (a.length() < b.length())
            return 3;
        //origin
        if (point1.eq(p2))
            return 4;
        //destination
        if (point2.eq(p2))
            return 5;
        //between
        return 6;
    };

    this.classifyByEdge = function (edge) {
        return this.classify(edge.org, edge.dest);
    };

    this.length = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };

    this.eq = function (point) {
        return this.x == point.x && this.y == point.y;
    };

    this.lessThan = function (point) {
        return this.x < point.x || (this.x == point.x && this.y < point.y);
    };

    this.moreThan = function (point) {
        return this.x > point.x || (this.x == point.x && this.y > point.y);
    };

    this.multiply = function (value) {
        return new Point(value * this.x, value * this.y);
    };
}

function Point3D(x, y, z, t) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.t = t;

    this.toInt = function () {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);
        this.z = Math.round(this.z);
        this.t = Math.round(this.t);
    };

    this.plus = function (point) {
        return new Point3D(this.x + point.x, this.y + point.y, this.z + point.z, 1);
    };

    this.devide = function (number) {
        return new Point3D(this.x / number, this.y / number, this.z / number, 1);
    };

    this.rotate = function (angle) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getRotateMatrixX(angle))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };

    this.rotateX = function (angle) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getRotateMatrixX(angle))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };

    this.rotateY = function (angle) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getRotateMatrixY(angle))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };

    this.rotateZ = function (angle) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getRotateMatrixZ(angle))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };


    this.scale = function (sx, sy, sz) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getScaleMatrix(sx, sy, sz))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };

    this.translate = function (dx, dy, dz) {
        var result = multiplyMatrix(this.getXYZMatrix(), this.getTranslateMatrix(dx, dy, dz))[0];
        return new Point3D(result[0], result[1], result[2], result[3]);
    };

    this.projection = function (viewWidth, viewHeight, viewDistance, fov) {
        var factor = fov / (viewDistance + this.z);
        var x = parseInt(this.x * factor + viewWidth / 2);
        var y = parseInt(this.y * factor + viewHeight / 2);
        return new Point3D(x, y, z, 1);
    };

    this.getTranslateMatrix = function (dx, dy, dz) {
        return [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [dx, dy, dz, 1]
        ]
    };

    this.getRotateMatrixX = function (angle) {
        var rad = angle * Math.PI / 180;
        var cosa = Math.cos(rad);
        var sina = Math.sin(rad);
        return [
            [1, 0, 0, 0],
            [0, cosa, sina, 0],
            [0, -sina, cosa, 0],
            [0, 0, 0, 1]
        ]
    };

    this.getRotateMatrixY = function (angle) {
        var rad = angle * Math.PI / 180;
        var cosa = Math.cos(rad);
        var sina = Math.sin(rad);
        return [
            [cosa, 0, -sina, 0],
            [0, 1, 0, 0],
            [sina, 0, cosa, 0],
            [0, 0, 0, 1]
        ]
    };

    this.getRotateMatrixZ = function (angle) {
        var rad = angle * Math.PI / 180;
        var cosa = Math.cos(rad);
        var sina = Math.sin(rad);
        return [
            [cosa, sina, 0, 0],
            [-sina, cosa, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]
    };

    this.getScaleMatrix = function (sx, sy, sz) {
        return [
            [sx, 0, 0, 0],
            [0, sy, 0, 0],
            [0, 0, sz, 0],
            [0, 0, 0, 1]
        ]
    };

    this.getXYZMatrix = function () {
        return [
            [this.x, this.y, this.z, this.t]
        ];
    };

    this.eq = function (point) {
        return this.x == point.x
            && this.y == point.y
            && this.z == point.z
            && this.t == point.t;
    };

    this.getVerticalMatrix = function () {
        return [
            [this.x],
            [this.y],
            [this.z],
            [this.t]
        ];
    };

    this.getHorizontalMatrix = function () {
        return [
            [this.x, this.y, this.z, this.t]
        ];
    }

}

function equalPoints(p1, p2) {
    if (p1.x == p2.x && p1.y == p2.y)
        return true;
    return false;

}

function multiplyMatrix(matrixA, matrixB) {

    var rowsA = matrixA.length,
        colsA = matrixA[0].length,
        rowsB = matrixB.length,
        colsB = matrixB[0].length,
        result = [];


    if (colsA != rowsB) return false;

    for (var i = 0; i < rowsA; i++) result[i] = [];

    for (var k = 0; k < colsB; k++) {
        for (var i = 0; i < rowsA; i++) {
            var temp = 0;
            for (var j = 0; j < rowsB; j++) temp += matrixA[i][j] * matrixB[j][k];
            result[i][k] = temp;
        }
    }

    return result;
}

function sign(value) {
    if (value < 0) {
        return -1;
    }
    else if (value > 0) {
        return 1;
    }
    else {
        return 0;
    }
}

function fpart(value) {
    return value - Math.floor(value);
}

function rfpart(value) {
    return 1 - fpart(value);
}

PlaneEquation = {
    getCoeffValue: function (xArray, yArray, zArray) {
        var x1 = xArray[0];
        var x2 = xArray[1];
        var x3 = xArray[2];

        var y1 = yArray[0];
        var y2 = yArray[1];
        var y3 = yArray[2];

        var z1 = zArray[0];
        var z2 = zArray[1];
        var z3 = zArray[2];

        return x1 * (y2 * z3 - z2 * y3) - x2 * (y1 * z3 - z1 * y3) + x3 * (y1 * z2 - z1 * y2);
    },

    getPlaneMatrix: function (point1, point2, point3) {
        var xArray = [point1.x, point2.x, point3.x];
        var yArray = [point1.y, point2.y, point3.y];
        var zArray = [point1.z, point2.z, point3.z];

        var Avalue = this.getCoeffValue([1, 1, 1], yArray, zArray);
        var Bvalue = this.getCoeffValue(xArray, [1, 1, 1], zArray);
        var Cvalue = this.getCoeffValue(xArray, yArray, [1, 1, 1]);
        var Dvalue = -this.getCoeffValue(xArray, yArray, zArray);

        return [
            [Avalue],
            [Bvalue],
            [Cvalue],
            [Dvalue]
        ];
    }


}

Side = function (sideMatrix) {
    this.points = [];
    this.matrix = sideMatrix;
    this.posOfPointRelativePlane = function (point) {
        var position = multiplyMatrix([
            [point.x, point.y, point.z, point.t]
        ], this.matrix);

        if (position == 0) return position;
        return (position / Math.abs(position));
    }

    this.A = this.matrix[0][0];
    this.B = this.matrix[1][0];
    this.C = this.matrix[2][0];
    this.D = this.matrix[3][0];

    this.fixCoeff = function (figureRelatedPoint) {
        var x = figureRelatedPoint.x;
        var y = figureRelatedPoint.y;
        var z = figureRelatedPoint.z;
        var result = this.A * x + this.B * y + this.C * z + this.D;
        var m =-sign(result);
        this.A *= m;
        this.B *= m;
        this.C *= m;
        this.D *= m;
    }
    //have to be like square
    this.addPoint = function (point) {
        this.points.push(point);
    }

    this.paint = function (drawer) {


        for (var i = 0; i <= this.points.length - 1; i++) {
            if (i == this.points.length - 1)
                drawer.line(this.points[i], this.points[0])
            else
                drawer.line(this.points[i], this.points[i + 1]);

        }
    }
}

function scalarMultiply(point1, point2) {
    return (point1.x * point2.x + point1.y * point2.y + point1.z * point2.z);
}

var Namespace = {
    pixelsCanvas: "pixelsCanvas",
    debugButton: "debugButton",
    nextStepButton: "nextStepButton",
    stopButton: "stopButton",
    parameters: "parameters",
    modeInfo: "modeInfo"

};

var Colors = {
    black: "#000000",
    white: "#ffffff",
    pink: "#FF7F7F"

};