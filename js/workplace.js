function Pixel() {
    this.color = "#FFFFFF";
	this.figure = null;
}

var WorkPlace = {
    workArea: [],
    width: 0,
    height: 0,
    scale: 5,

    translate: function (physPoint) {
        //incorrect Y when scale <= 2
        return new Point(parseInt(physPoint.x / (this.scale + this.offset())), parseInt(physPoint.y / (this.scale + this.offset())) );
    },

    offset: function () {
        if (this.scale <= 3)
            return 0;
        else
            return 1;
    },

    create: function (width, height) {
        this.workArea = [];

        this.width = parseInt(width);
        this.height = parseInt(height);

//        console.log("Creating work area: " + this.width + "x" + this.height);
        for (var i = 0; i < this.width; i++) {
            var widthPixels = [];
            for (var j = 0; j < this.height; j++) {
                widthPixels.push(new Pixel());
            }
            this.workArea.push(widthPixels);
        }
    },
	
	clear: function() {
		for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                this.workArea[i][j].color = "#FFFFFF";
			}
		}
	},

    getPixelColor: function (x, y) {
        return this.workArea[x][y].color;
    },

    //drawing from the top left corner
    putPixel: function (x, y, color) {
        y = parseInt(y);
        x = parseInt(x);

        if (isCorrectIndex(x, this.workArea)) {
            var heightPixels = this.workArea[x];
            if (isCorrectIndex(y, heightPixels)) {
                var pixel = heightPixels[y];
                pixel.color = color;
            }
        }
    },

    createCanvas: function () {
        var canvas = document.getElementById(Namespace.pixelsCanvas)

        var offset = this.offset()

        var commonScale = this.scale + offset

        canvas.width = this.width * commonScale + offset
        canvas.height = this.height * commonScale + offset
        canvas.hidden = false
    },

    drawPixels: function () {
        var canvas = document.getElementById(Namespace.pixelsCanvas)

        var canvasContext = canvas.getContext("2d")

        var offset = this.offset()

        var x = offset
        var y = offset
        var scale = this.scale

        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                var pixel = this.workArea[j][i]
                canvasContext.fillStyle = pixel.color
                canvasContext.fillRect(x, y, scale, scale)
                x = x + scale + offset
            }
            x = offset
            y = y + scale + offset
        }
    },

    drawOnePixel: function (i, j, color) {
        this.putPixel(i, j, color);

        var canvas = document.getElementById(Namespace.pixelsCanvas);
        var canvasContext = canvas.getContext("2d");
        var offset = this.offset();
        var scale = this.scale;

        var x = offset + i * (scale + offset);
        var y = offset + j * (scale + offset);

        canvasContext.fillStyle = color;
        canvasContext.fillRect(x, y, scale, scale);
    },

    repaint: function () {
        this.createCanvas()
        this.drawPixels()
    },

    changeScale: function (newScale) {
        if (newScale >= 1 && newScale <= 20) {
            this.scale = parseInt(newScale)
            this.repaint()
        }
    }
};


function isCorrectIndex(index, array) {
    var arrayLen = array.length;
    if (index < arrayLen && index >= 0)
        return true;
    return false;
}